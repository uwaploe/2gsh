// 2gsh provides an interactive shell for testing a 2G Actuator
package main

import (
	"bufio"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"text/template"

	"bitbucket.org/uwaploe/twog"
	"github.com/c-bata/go-prompt"
	"github.com/k0kubun/go-ansi"
	"github.com/mattn/go-shellwords"
)

const Usage = `Usage: 2gsh [options]

Interactive shell to test a 2G actuator.
`

var Version = "dev"
var BuildDate = "unknown"

const PROMPT = ">> "

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	debugMode = flag.Bool("debug", false,
		"Show I/O debugging information")
	devAddr string = "10.40.7.13:10001"
	logFile string = os.DevNull
)

type completer struct {
	cmds map[string]Commander
}

type executor struct {
	cmds map[string]Commander
}

func (c *completer) complete(d prompt.Document) []prompt.Suggest {
	bc := d.TextBeforeCursor()
	if bc == "" {
		return nil
	}

	args := strings.Split(bc, " ")

	var s []prompt.Suggest
	cmd, ok := c.cmds[args[0]]
	if !ok {
		if len(args) == 1 {
			// number of commands + help
			s = make([]prompt.Suggest, len(c.cmds)+1)
			s = append(s,
				prompt.Suggest{Text: "help", Description: "show help message"})
			s = append(s,
				prompt.Suggest{Text: "quit", Description: "exit the shell"})
			for name, cmd := range c.cmds {
				s = append(s,
					prompt.Suggest{Text: name, Description: cmd.Synopsis()})
			}
		}
	} else {
		s = cmd.Completion()
	}

	return prompt.FilterHasPrefix(s, d.GetWordBeforeCursor(), true)
}

func (e *executor) execute(l string) bool {
	if l == "quit" || l == "exit" {
		return true
	}

	// Ignore spaces
	if len(strings.TrimSpace(l)) == 0 {
		return false
	}

	parts, err := shellwords.Parse(l)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		return false
	}

	if parts[0] == "help" {
		showHelp(os.Stdout, e.cmds, parts)
		return false
	}

	cmd, ok := e.cmds[parts[0]]
	if !ok {
		fmt.Fprintf(os.Stderr, "Unknown command: %q\n", parts[0])
		return false
	}

	var args []string
	if len(parts) != 1 {
		args = parts[1:]
	}

	if err := cmd.Validate(args); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		fmt.Fprint(os.Stderr, cmd.Help())
		return false
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Catch signals
	go func() {
		<-sigs
		cancel()
	}()

	result, err := cmd.Run(ctx, args)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
	} else {
		if result != "" {
			fmt.Fprintln(os.Stdout, result)
		}
	}

	return false
}

func showHelp(w io.Writer, cmds map[string]Commander, cmdline []string) {
	if len(cmdline) > 1 {
		name := cmdline[1]
		if cmd, ok := cmds[name]; ok {
			fmt.Fprintln(w, cmd.Help())
			return
		}
	}

	var maxLen int
	// slice of [name, synopsis]
	text := make([][]string, 0, len(cmds))
	for name, cmd := range cmds {
		text = append(text, []string{name, cmd.Synopsis()})
		if len(name) > maxLen {
			maxLen = len(name)
		}
	}

	text = append(text, []string{"quit", "exit program"})
	if maxLen < 4 {
		maxLen = 4
	}

	var cmdText string
	for _, e := range text {
		cmdText += fmt.Sprintf("  %-"+strconv.Itoa(maxLen)+"s    %s\n",
			e[0], e[1])
	}

	fmt.Fprintf(w, "Available commands:\n%s", cmdText)
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&devAddr, "addr", devAddr,
		"Actuator network address (HOST:PORT)")
	flag.StringVar(&logFile, "log", logFile,
		"File to log status during actuator movements")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func repl(dev *twog.Device) error {
	cfg, err := dev.Configuration()
	if err != nil {
		return err
	}
	b, _ := json.Marshal(cfg)
	fmt.Printf("System configuration: %s\n", b)

	logf, err := os.OpenFile(logFile, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return fmt.Errorf("Cannot open %q: %w", logFile, err)
	}
	defer logf.Close()

	cmds := map[string]Commander{
		"status":  &statCommand{dev: dev},
		"power":   &powerCommand{dev: dev},
		"goto":    &gotoCommand{dev: dev, cfg: cfg, wtr: logf},
		"monitor": &monCommand{dev: dev},
		"faults":  &faultCommand{dev: dev},
		"limits":  &limitCommand{dev: dev},
	}

	executor := &executor{cmds: cmds}
	completer := &completer{cmds: cmds}
	defer ansi.CursorShow()

	var input string
	for {
		ansi.CursorShow()
		if runtime.GOOS == "windows" {
			// on windows, use a plain input prompt,
			// as the ANSI codes from the prompt package
			// are not well supported
			fmt.Print(PROMPT)
			scanner := bufio.NewScanner(os.Stdin)
			scanner.Scan()
			input = scanner.Text()
		} else {
			input = prompt.Input(PROMPT, completer.complete)
		}
		ansi.CursorHide()
		if executor.execute(input) {
			break
		}
	}

	return nil
}

var FwTmpl = `Device Information
FW Version (build): {{.Version}} ({{.BuildNum}})
Date: {{.BuildDate}}
Serial#: {{.Sn}}

`

func main() {
	parseCmdLine()

	if *debugMode {
		twog.TraceFunc = log.Printf
	}

	conn, err := net.Dial("tcp", devAddr)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	dev := twog.NewDevice(conn, "actuator-1")
	defer dev.SetState(twog.MotorOff)

	info, err := dev.SysInfo()
	if err != nil {
		log.Print(err)
		return
	}

	fmt.Printf("** 2G Actuator Test Shell (%s, %s) **\n** Type help to get a list of commands\n\n",
		Version, BuildDate)
	tmpl, err := template.New("fw").Parse(FwTmpl)
	if err == nil {
		tmpl.Execute(os.Stdout, info)
	}

	err = repl(dev)
	if err != nil {
		log.Print(err)
	}
}

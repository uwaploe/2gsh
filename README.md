# Test 2G Actuators

2gsh is a simple interactive shell to test 2G Actuators.

## Usage

``` shellsession
sysop@invader-sic-1:~$ 2gsh --help
Usage: 2gsh [options]

Interactive shell to test a 2G actuator.
  -addr string
        Actuator network address (HOST:PORT) (default "10.40.7.13:10001")
  -debug
        Show I/O debugging information
  -log string
        File to log status during actuator movements (default "/dev/null")
  -version
        Show program version information and exit
```

When the program starts, it displays the device firmware revision and serial number along with a JSOn encoded version of the current configuration (see the actuator documentation for details).

``` shellsession
sysop@invader-sic-1:~$ 2gsh
** 2G Actuator Test Shell (0.7.0, 2020-04-20T22:54:06Z) **
** Type help to get a list of commands

Device Information
FW Version (build): 6.13 (5090)
Date: 2020-01-24 22:13:35 +0000 UTC
Serial#: 1300300d-aecd8880-572cd93b-f50020c6

System configuration: {"pgain":4,"igain":1,"dgain":0.105,"perr":12,"dclamp":100,"limit_period":3000,"max_match":2900,"pos_high":8000,"pos_low":0,"power_type":1}
>>
```

Commands are entered at the `>>` prompt.

``` shellsession
>> help
Available commands:
  status     return actuator status information
  power      turn motor power on or off
  goto       move to a setpoint in mils
  monitor    print device status at regular intervals, stop with ctrl-c
  faults     return actuator fault information
  limits     get/set current limit parameters
  quit       exit program
>>
```

The shell supports [command completion](https://en.wikipedia.org/wiki/Command-line_completion), press the TAB key at any time to display a list of possible values.

## Commands

### status

Status displays the current device status.

``` shellsession
>> status
{"state":0,"dir":0,"pos":2000,"temp":[28,28],"v":24232,"i":32}
>>
```

*State* is the motor state (0=off, 1=on), *pos* is the position in mils, *temp* is the temperature in degrees C (two sensors), *v* is the voltage in millivolts, and *i* is the current in milliamps.

### power on|off

Power switches the motor power on or off. the power is automatically switched off when the shell exits.

### goto [--vel=MIL/MIN] [--threshold=MIL] setpoint

Move to an absoulte setpoint in mils. Command line options can be used to specify the velocity, timeout, and setpoint threshold. If this command is interrupted with *ctrl-c*, the motor will be powered off.

| **Option**  | **Description**            | **Default** |
|-------------|----------------------------|-------------|
| --vel       | Velocity in mils/min       | 10000       |
| --threshold | Position threshold in mils | 0           |

The motor must be powered on before running this command.

``` shellsession
>> goto 2010
ERROR: Motor must be powered on
>> power on
>> goto 2010
>> status
{"state":1,"dir":1,"pos":2010,"temp":[32,32],"v":24232,"i":70}
>>
```

### monitor [interval]

Monitor periodically polls the actuator status and displays the result until interrupted with *ctrl-c*. The default polling interval is 1s. This command can be used to see how well the actuator is holding its setpoint.

``` shellsession
>> monitor
Enter ctrl-c to stop ...
{"state":1,"dir":1,"pos":2010,"temp":[32,32],"v":24232,"i":67}
{"state":1,"dir":1,"pos":2010,"temp":[32,32],"v":24232,"i":67}
{"state":1,"dir":1,"pos":2009,"temp":[32,32],"v":24232,"i":70}
{"state":1,"dir":1,"pos":2009,"temp":[32,32],"v":24219,"i":67}
{"state":1,"dir":1,"pos":2009,"temp":[32,32],"v":24232,"i":70}
{"state":1,"dir":1,"pos":2010,"temp":[32,32],"v":24232,"i":67}
{"state":1,"dir":1,"pos":2010,"temp":[32,32],"v":24232,"i":67}
{"state":1,"dir":1,"pos":2009,"temp":[32,32],"v":24232,"i":70}
{"state":1,"dir":1,"pos":2010,"temp":[32,32],"v":24232,"i":67}
{"state":1,"dir":1,"pos":2010,"temp":[33,33],"v":24219,"i":70}
{"state":1,"dir":1,"pos":2010,"temp":[32,32],"v":24219,"i":67}
{"state":1,"dir":1,"pos":2010,"temp":[33,33],"v":24232,"i":67}
^C>>
```

### faults

Faults returns the system fault information. Each value is a bitmask, see the actuator manual for details (0 means no faults).

``` shellsession
>> faults
{"motor":0,"pos":0,"temp":0,"comm":0}
>>
```

## Status Logging

The *--log* command-line option is used to specify a log file for the actuator status during every movement. The file format is [newline-delimited JSON](http://jsonlines.org), each record (line) has the following format:

``` json
{
  "time": "2020-04-21T16:08:22.260874641Z",
  "source": "actuator-1",
  "data": {
    "state": 1,
    "dir": 0,
    "pos": 2008,
    "temp": [
      30,
      30
    ],
    "v": 24232,
    "i": 52
  }
}
```

The `jq` program can be used to convert the file to CSV.

``` shellsession
sysop@invader-sic-1:~$ jq -r '[.time, .data.pos, .data.temp, .data.v, .data.i]|flatten|@csv' < stat.ndjson > stat.csv
sysop@invader-sic-1:~$ head -n 2 stat.csv
"2020-04-21T16:08:22.260874641Z",2008,30,30,24232,52
"2020-04-21T16:08:22.266633254Z",2008,30,30,24232,52
sysop@invader-sic-1:~$
```

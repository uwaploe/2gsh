module bitbucket.org/uwaploe/2gsh

go 1.13

require (
	bitbucket.org/uwaploe/twog v0.2.0
	github.com/briandowns/spinner v1.10.0
	github.com/c-bata/go-prompt v0.2.3
	github.com/k0kubun/go-ansi v0.0.0-20180517002512-3bf9e2903213
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mattn/go-shellwords v1.0.10
	github.com/pkg/term v0.0.0-20190109203006-aa71e9d9e942 // indirect
)

replace bitbucket.org/uwaploe/twog => ../pkg/twog

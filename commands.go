package main

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"strconv"
	"time"

	"bitbucket.org/uwaploe/twog"
	"github.com/briandowns/spinner"
	"github.com/c-bata/go-prompt"
)

type dataRecord struct {
	T    time.Time   `json:"time"`
	Src  string      `json:"source"`
	Data interface{} `json:"data"`
}

type Commander interface {
	Help() string
	Synopsis() string
	Validate(args []string) error
	Completion() []prompt.Suggest
	Run(ctx context.Context, args []string) (string, error)
}

type statCommand struct {
	dev *twog.Device
}

func (c *statCommand) Help() string {
	return "usage: status"
}

func (c *statCommand) Synopsis() string {
	return "return actuator status information"
}

func (c *statCommand) Validate(args []string) error {
	return nil
}

func (c *statCommand) Completion() []prompt.Suggest {
	return nil
}

func (c *statCommand) Run(ctx context.Context, args []string) (string, error) {
	stat, err := c.dev.Status()
	if err != nil {
		return "", err
	}
	b, err := json.Marshal(stat)
	return string(b), nil
}

type powerCommand struct {
	dev   *twog.Device
	state twog.MotorState
}

func (c *powerCommand) Help() string {
	return "usage: power on|off"
}

func (c *powerCommand) Synopsis() string {
	return "turn motor power on or off"
}

func (c *powerCommand) Validate(args []string) error {
	if len(args) == 0 {
		return nil
	}
	switch args[0] {
	case "on", "ON":
		c.state = twog.MotorOn
	case "off", "OFF":
		c.state = twog.MotorOff
	default:
		return fmt.Errorf("invalid power state: %q", args[0])
	}

	return nil
}

func (c *powerCommand) Completion() []prompt.Suggest {
	return []prompt.Suggest{
		{Text: "on", Description: "turn motor on"},
		{Text: "off", Description: "turn motor off"},
	}
}

func (c *powerCommand) Run(ctx context.Context, args []string) (string, error) {
	return "", c.dev.SetState(c.state)
}

type gotoCommand struct {
	dev     *twog.Device
	p       twog.PV
	cfg     twog.SystemConfig
	timeout time.Duration
	wtr     io.Writer
}

func moveToSetpoint(ctx context.Context, dev *twog.Device,
	p twog.PV, wtr io.Writer) error {
	enc := json.NewEncoder(wtr)

	err := dev.Goto(p)
	if err != nil {
		return fmt.Errorf("Cannot start motion: %w", err)
	}

	s := spinner.New(spinner.CharSets[9], 100*time.Millisecond)
	s.Prefix = "Waiting for motion to complete ... "

	ch := make(chan error, 1)
	stch := make(chan twog.Status, 1)

	go func() { ch <- dev.WaitForSetpoint(ctx, p.Position, stch) }()

	s.Start()
	defer s.Stop()
	for {
		select {
		case err = <-ch:
			return err
		case stat := <-stch:
			enc.Encode(dataRecord{
				T:    time.Now(),
				Src:  dev.Name(),
				Data: stat})
		}
	}

	return err
}

func (c *gotoCommand) Help() string {
	return "usage: goto [--vel=MIL/MIN] [--threshold=MIL] setpoint"
}

func (c *gotoCommand) Synopsis() string {
	return "move to a setpoint in mils"
}

func (c *gotoCommand) Validate(args []string) error {
	fs := flag.NewFlagSet("goto", flag.ContinueOnError)
	fs.DurationVar(&c.timeout, "timeout", 0,
		"maximum time allowed for motion")
	vel := fs.Uint("vel", 10000, "velocity in mil/min")
	threshold := fs.Uint("threshold", 0, "setpoint threshold in mils")
	err := fs.Parse(args)
	if err != nil {
		return err
	}

	c.p.Velocity = uint32(*vel)
	c.p.Threshold = uint32(*threshold)
	x, err := strconv.Atoi(fs.Arg(0))
	if err != nil {
		return err
	}

	if x > int(c.cfg.PosHigh) || x < int(c.cfg.PosLow) {
		return fmt.Errorf("Setpoint out of range: %d", x)
	}
	c.p.Position = int32(x)

	return nil
}

func (c *gotoCommand) Completion() []prompt.Suggest {
	return []prompt.Suggest{
		{Text: "--vel", Description: "set velocity in mil/min"},
		{Text: "--threshold", Description: "set position threshold in mils"},
	}
}

func (c *gotoCommand) Run(ctx context.Context, args []string) (string, error) {
	stat, err := c.dev.Status()
	if err != nil {
		return "", err
	}

	if (stat.State & twog.MotorOn) != twog.MotorOn {
		return "", errors.New("Motor must be powered on")
	}

	var cancel context.CancelFunc
	if c.timeout > 0 {
		ctx, cancel = context.WithTimeout(ctx, c.timeout)
		defer cancel()
	}

	err = moveToSetpoint(ctx, c.dev, c.p, c.wtr)
	if errors.Is(err, context.Canceled) {
		if err := c.dev.SetState(twog.MotorOff); err == nil {
			fmt.Println("Interrupt; motor powered off")
		}
	}
	return "", err
}

type monCommand struct {
	dev      *twog.Device
	interval time.Duration
}

func (c *monCommand) Help() string {
	return "usage: monitor [interval]"
}

func (c *monCommand) Synopsis() string {
	return "print device status at regular intervals, stop with ctrl-c"
}

func (c *monCommand) Validate(args []string) error {
	var err error
	if len(args) > 1 {
		c.interval, err = time.ParseDuration(args[0])
		if err != nil {
			return fmt.Errorf("Invalid interval: %q", args[0])
		}
	} else {
		c.interval = time.Second
	}

	return nil
}

func (c *monCommand) Completion() []prompt.Suggest {
	return nil
}

func (c *monCommand) Run(ctx context.Context, args []string) (string, error) {
	errch := make(chan error, 1)
	stch := make(chan twog.Status, 1)

	fmt.Println("Enter ctrl-c to stop ...")
	go func() { errch <- c.dev.PollStatus(ctx, c.interval, stch) }()

	for {
		select {
		case err := <-errch:
			if err == context.Canceled {
				err = nil
			}
			return "", err
		case stat := <-stch:
			b, _ := json.Marshal(stat)
			fmt.Println(string(b))
		}
	}

	return "", nil
}

type faultCommand struct {
	dev *twog.Device
}

func (c *faultCommand) Help() string {
	return "usage: fault"
}

func (c *faultCommand) Synopsis() string {
	return "return actuator fault information"
}

func (c *faultCommand) Validate(args []string) error {
	return nil
}

func (c *faultCommand) Completion() []prompt.Suggest {
	return nil
}

func (c *faultCommand) Run(ctx context.Context, args []string) (string, error) {
	faults, err := c.dev.Faults()
	if err != nil {
		return "", err
	}

	b, _ := json.Marshal(faults)
	return string(b), nil
}

type limitCommand struct {
	dev    *twog.Device
	limits twog.Limits
}

func (c *limitCommand) Help() string {
	return "usage: limit [--actuator=AMPS] [--fallback=%] [--motor=AMPS]"
}

func (c *limitCommand) Synopsis() string {
	return "get/set current limit parameters"
}

func (c *limitCommand) Completion() []prompt.Suggest {
	return []prompt.Suggest{
		{Text: "--actuator", Description: "actuator board current limit (amps)"},
		{Text: "--fallback", Description: "throttle % when limit is reached"},
		{Text: "--motor", Description: "motor current limit (amps)"},
	}
}

func (c *limitCommand) Validate(args []string) error {
	var err error

	if len(args) == 0 {
		return nil
	}

	// Use the current settings as default values
	c.limits, err = c.dev.Limits()
	if err != nil {
		return err
	}

	fs := flag.NewFlagSet("limits", flag.ContinueOnError)
	actuator := fs.Float64("actuator", float64(c.limits.Actuator),
		"actuator board current limit (amps)")
	fallback := fs.Float64("fallback", float64(c.limits.Fallback),
		"throttle back amount when limit is reached (%)")
	motor := fs.Float64("motor", float64(c.limits.Motor),
		"motor current limit (amps)")
	err = fs.Parse(args)
	if err != nil {
		return err
	}

	c.limits.Actuator = float32(*actuator)
	c.limits.Fallback = float32(*fallback)
	c.limits.Motor = float32(*motor)

	return nil
}

func (c *limitCommand) Run(ctx context.Context, args []string) (string, error) {
	if len(args) == 0 {
		l, err := c.dev.Limits()
		if err != nil {
			return "", err
		}
		b, _ := json.Marshal(l)
		return string(b), nil
	}

	err := c.dev.SetLimits(c.limits)
	return "", err
}
